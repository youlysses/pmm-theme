;; pmm-theme.el -- A customize theme, for how I imagine a moderized LispM would look...
;; Copyright (C) 2013, Joshua "Youlysses" S. Grant

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


(deftheme postmodernmannerism 
  "Theme to somewhat replicate and moderinize the little I've seen from the OpenGenera System.")

(let ((class '((class color) (min-colors 89))))
  (custom-theme-set-faces
   'adwaita

   '(cursor ((t (:background "grey65"))))
   '(region ((t (:background "grey82" :foreground "black")))) 
   '(border-color ((t (:background "#EDEDED"))))
   '(foreground-color ((t (:background "black"))))
   '(backgound-color ((t (:background "#EDEDED"))))
   '(default ((t (:background "#EDEDED" :foreground "#000000"))))
   '(fringe ((t (:background "#EDEDED"))))
   '(mode-line ((t :background "black" :foreground "white")))
   '(mode-line-inactive ((t (:foreground "#C6C6C6" :background "grey90"))))
   '(header-line ((t (:background "grey87" :foreground "grey50"))))
   `(widget-button ((t (:bold t :foreground "#0084AA"))))

   `(minibuffer-prompt ((t (:foreground "#A87CCC" :bold t))))
   `(ido-first-match ((t (:foreground "black" :bold t))))
   `(ido-only-match ((t (:foreground "black" :bold t))))
   `(ido-subdir ((t (:foreground "SlateBlue"))))
   `(ido-virtual ((t (:foreground "green"))))

   `(font-lock-builtin-face ((t (:foreground "black" :bold t))))
   `(font-lock-comment-face ((t (:foreground "grey50"))))
   `(font-lock-constant-face ((t (:foreground "#F5666D"))))
   `(font-lock-doc-face ((t (:foreground "grey50" :bold t))))
   `(font-lock-function-name-face ((t (:foreground "#F74D99" :bold t))))
   `(font-lock-keyword-face ((t (:foreground "black" :bold t))))
   `(font-lock-preprocessor-face ((t (:foreground "black"))))
   `(font-lock-string-face ((t (:foreground "#FF7092"))))
   `(font-lock-type-face ((t (:foreground "#F74D99" :bold t))))
   `(font-lock-variable-name-face ((t (:foreground "#A8799C" :bold t))))
   `(font-lock-warning-face ((t (:foreground "#F5666D" :bold t))))

;; DIRED

   `(dired-header ((t (:foreground "#A87CCC" :bold t))))
   `(dired-directory ((t (:foreground "SlateBlue" :bold t))))
   `(dired-flagged ((t (:foreground "red"))))
   `(dired-ignored ((t (:foreground "grey50"))))
   `(dired-mark ((t (:foreground "#A87CCC" :bold t))))
   `(dired-marked ((t (:foreground "orange"))))
;  `(dired-perm-write
   `(dired-syslink ((t (("SlateBlue")))))
;  `(dired-warning
;  `(

;; Add help and info ...


;; W3M

  `(w3m-tab-background ((t (:background "grey85"))))
  `(w3m-tab-selected ((t :background "#EDEDED" :foreground "#ec4")))

  `(w3m-header-line-location-content ((t (:foreground "grey55"))))
  `(w3m-header-line-location-title ((t (:foreground "#A8799C" :bold t))))

  `(w3m-anchor ((t (:foreground "black" :underline t )))) 
  `(w3m-arrived-anchor ((t (:foreground "grey20" :underline t))))

;; `(w3m-current-anchor

 `(w3m-form ((t (:foreground "red"))))
 `(w3m-form-button ((t (:background "red"))))
 `(w3m-form-button-mouse  ((t (:background "blue"))))

;; `(w3m-history-current-url
;; `(w3m-image
;; `(w3m-image-anchor

;;   `(w3m-bold
;;   `(w3m-insert
;;   `(w3m-italic
;;   `(w3m-underline
;;   `(w3m-strike-through-face



  `(org-todo ((t (:foreground "red"))))
  `(org-done ((t (:foreground "LimeGreen"))))
  `(org-ellipsis ((t (:foreground "black"))))

  ; `(org-link ((t (:foreground ""))))

  `(org-document-info-keyword ((t (:foreground "black" :bold t))))
  `(org-document-info ((t (:foreground "black" :bold t))))
  `(org-document-title ((t (:foreground "black" :bold t))))
  `(org-meta-line ((t (:foreground "grey50"))))

  `(org-level-1 ((t (:foreground "#F74D99" :bold t))))
  `(org-level-2 ((t (:foreground "black" :bold t))))
  `(org-level-3 ((t (:foreground "black" :bold t))))
  `(org-level-4 ((t (:foreground "black" :bold t))))
  `(org-level-5 ((t (:foreground "black" :bold t))))
  `(org-level-6 ((t (:foreground "black" :bold t))))
  `(org-level-7 ((t (:foreground "black" :bold t))))
  `(org-level-8 ((t (:foreground "black" :bold t))))

;; ERC

   `(erc-action-face ((t (:foreground "#F5666D"))))
   `(erc-button ((t (:foreground "grey55" :italic t))))
   `(erc-current-nick-face ((t (:bold t :foreground "black" :bold t))))
   `(erc-error-face ((t (:foreground "red" :bold t))))
   `(erc-input-face ((t (:foreground "black"))))
   `(erc-keyword-face ((t (:foreground "#F5666D"))))
   `(erc-my-nick-face ((t (:bold t :foreground "black"))))
   `(erc-nick-default-face ((t (:bold t :foreground "black"))))
   `(erc-notice-face ((t (:foreground "SlateBlue2"))))
   `(erc-prompt-face ((t (:foreground "#A87CCC" :bold t))))
   `(erc-timestamp-face ((t (:foreground "#9CBB43"))))

;; GNUS

   ;; `(gnus-group-mail-1-empty ((t (:foreground "#000000"))))
   ;; `(gnus-group-mail-1 ((t (:bold t :foreground "#000000"))))
   ;; `(gnus-group-mail-3-empty ((t (:foreground "#000000"))))
   ;; `(gnus-group-mail-3 ((t (:bold t :foreground "#000000"))))
   ;; `(gnus-group-news-3-empty ((t (:foreground "#000000"))))
   ;; `(gnus-group-news-3 ((t (:bold t :foreground "#000000"))))
   ;; `(gnus-header-name ((t (:bold t :foreground "#A8799C"))))
   ;; `(gnus-header-subject ((t (:bold t :foreground "grey55"))))
   ;; `(gnus-header-content ((t (:foreground "grey55" :bold t))))
   ;; `(gnus-button ((t (:bold t :foreground "#000000"))))
   ;; `(gnus-cite-1 ((t (:foreground "#000000"))))
   ;; `(gnus-cite-2 ((t (:foreground "#000000"))))

;; EMMS

   `(emms-browser-artist-face ((t (:foreground "purple2" :bold t))))
   `(emms-browser-album-face ((t (:foreground "SlateBlue2" :bold t))))
   ;; `(emms-browser-composer-face ((t (:foreground ""))))
   ;; `(emms-browser-performer-face ((t (:foreground ""))))
   `(emms-browser-year/genre-face ((t (:foreground "#9CBB43" :bold t ))))
   ;; `(emms-metaplaylist-mode-current-face ((t (:foreground ""))))
   `(emms-browser-track-face ((t (:foreground "SlateGrey"))))
   `(emms-playlist-selected-face ((t (:bold t :foreground "orange2" ))))
   `(emms-playlist-track-face ((t (:foreground "SlateGrey"))))
   `(emms-stream-name-face ((t (:foreground "purple2"))))
   `(emms-stream-url-face ((t (:foreground "SlateBlue2"))))

;; VC

   `(diff-added ((t (:bold t :foreground ,"#4CB64A"))))
   `(diff-removed ((t (:bold t :foreground "#F5666D"))))
))

;; Local Variables:
;; no-byte-compile: t
;; End:

;;; pmm-theme.el  ends here
